export default class AddUser extends React.Component {

    constructor(props) {

        super(props);
        this.addNewUser = this.addNewUser.bind(this);

    }

    addNewUser() {

        this.props.addNewUser(

            this.nomInput.value,
            this.prenomInput.value,
            this.mailInput.value,
            this.dateInput.value,
            this.pwdInput.value,
            this.pwdConfInput.value,
        
        );

    }

    render() {

        return (

            <form onSubmit={this.addNewUser}>

                <div className="form-group">
                    <label htmlFor="nom">Nom</label>
                    <input ref={nomInput => this.nomInput = nomInput} type="text" className="form-control" id="nom" placeholder="Entrer votre nom" required />
                </div>

                <div className="form-group">
                    <label htmlFor="prenom">Prenom</label>
                    <input ref={prenomInput => this.prenomInput = prenomInput} type="text" className="form-control" id="prenom" placeholder="Entrer votre prenom" required />
                </div>

                <div className="form-group">
                    <label htmlFor="email">Adresse email</label>
                    <input ref={mailInput => this.mailInput = mailInput} type="email" className="form-control" id="email" placeholder="Entrer votre adresse mail" required />
                </div>

                <div className="form-group">
                    <label htmlFor="date">Date de naissance</label>
                    <input ref={dateInput => this.dateInput = dateInput} type="date" className="form-control" id="date" placeholder="Entrer votre date de naissance" required />
                </div>

                <div className="form-group">
                    <label htmlFor="pwd">Password</label>
                    <input ref={pwdInput => this.pwdInput = pwdInput} type="password" className="form-control" id="pwd" placeholder="Password" required />
                </div>

                <div className="form-group">
                    <label htmlFor="pwdConf">Password confirmation</label>
                    <input ref={pwdConfInput => this.pwdConfInput = pwdConfInput} type="password" className="form-control" id="pwdConf" placeholder="Password confirmation" required />
                </div>

                <button type="submit" className="btn btn-dark pull-left">Submit</button>
            </form>

        )

    }

}