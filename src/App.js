import UserList from './UserList.js';
import AddUser from './AddUser.js';
import EditUser from './EditUser.js';

export default class App extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            userList: [],
            isEdit: false,
            currentUser: { id: '', nomEdit: '', prenom: '', mail: '', date: '', password: '', role: '' },
        }

        this.users = [
            { id: 1, nom: 'John', prenom: 'Doe', mail: 'Johndoe@test.com', date: '11/12/2122', password: 'test123', role: 1 },
            { id: 2, nom: 'Jane', prenom: 'Doe', mail: 'Janedoe@test.com', date: '11/12/2122', password: 'test123', role: 2 },
        ];

        this.editUser = this.editUser.bind(this);
        this.editUserSubmit = this.editUserSubmit.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.addNewUser = this.addNewUser.bind(this);

    }

    componentDidMount() {

        document.getElementById('alertConfPwd').hidden = true;

        document.getElementById('alertConfPwdEdit').hidden = true;

        let userList = this.users;

        this.setState((prevState, props) => ({
            userList: userList
        }));

    }

    addNewUser(nom, prenom, mail, date, pwd, pwdConf) {

        document.getElementById('alertConfPwd').hidden = true;

        document.getElementById('alertConfPwdEdit').hidden = true;

        // Not reload

        event.preventDefault();

        // Format date

        const newDate = new Date(date);

        const dateFormat = newDate.getDate() + '/' + (1 + newDate.getMonth()).toString().padStart(2, '0') + '/' + newDate.getFullYear();

        if (pwd === pwdConf) {

            this.setState((prevState, props) => ({

                userList: [...prevState.userList, {

                    id: prevState.userList.length + 1,
                    nom: nom,
                    prenom: prenom,
                    mail: mail,
                    date: dateFormat,
                    password: pwd,
                    role: 1

                }]

            }));

        }
        else {
            document.getElementById('alertConfPwd').hidden = false;
        }

    }

    deleteUser(id) {

        document.getElementById('alertConfPwd').hidden = true;

        document.getElementById('alertConfPwdEdit').hidden = true;

        let alert = window.confirm("Voulez-vous supprimer cet utilisateur ?");

        if (alert === true) {

            // Return all users except user deleted

            let filteredUserList = this.state.userList.filter(
                user => user.id !== id
            );

            this.setState((prevState, props) => ({
                userList: filteredUserList
            }));

        }

    }

    editUser(user) {

        this.setState((prevState, props) => ({
            isEdit: !prevState.isEdit,
            currentUser: user
        }));

        $('#formModal').modal('show');

    }

    editUserSubmit(nom, prenom, mail, date, pwd, pwdConf) {

        document.getElementById('alertConfPwd').hidden = true;

        document.getElementById('alertConfPwdEdit').hidden = true;

        event.preventDefault();

        if(pwd === pwdConf){

            const newDate = new Date(date);

            const dateFormat = newDate.getDate() + '/' + (1 + newDate.getMonth()).toString().padStart(2, '0') + '/' + newDate.getFullYear();
    
            let newUserList = this.state.userList.map((user) => {
    
                if (user.id === this.state.currentUser.id) {
    
                    user.nom = nom;
                    user.prenom = prenom;
                    user.mail = mail;
                    user.date = dateFormat;
                    user.password = pwd;
    
                }
    
                return user;
    
            });
    
            this.setState((prevState, props) => ({
                userList: newUserList,
                isEdit: !prevState.isEdit,
            }));

            $('#formModal').modal('hide');

        }

        else {
            document.getElementById('alertConfPwdEdit').hidden = false;
        }


    }

    render() {

        return (

            <div className="container mt-5">

                <h2>CRUD users</h2>

                <table className="table">

                    <thead>

                        <tr>

                            <th scope="col">#</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Prénom</th>
                            <th scope="col">Mail</th>
                            <th scope="col">Date de naissance</th>
                            <th scope="col">Role</th>
                            <th scope="cole">Edit</th>
                            <th scope="cole">Delete</th>

                        </tr>

                    </thead>

                    <UserList
                        deleteUser={this.deleteUser}
                        userList={this.state.userList}
                        editUser={this.editUser}
                    />

                </table>

                <div className="alert alert-warning" role="alert" id='alertConfPwd'>
                    Les mots de passe sont différents
                </div>

                <AddUser
                    addNewUser={this.addNewUser}
                />

                <EditUser
                    editUserSubmit={this.editUserSubmit}
                    currentUser={this.state.currentUser}
                />

            </div>

        );

    }

}