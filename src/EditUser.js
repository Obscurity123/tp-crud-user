export default class EditUser extends React.Component {

    constructor(props) {

        super(props);
        this.editUserSubmit = this.editUserSubmit.bind(this);

    }

    editUserSubmit() {

        this.props.editUserSubmit(

            this.nomEditInput.value,
            this.prenomEditInput.value,
            this.mailEditInput.value,
            this.dateEditInput.value,
            this.pwdEditInput.value,
            this.pwdConfEditInput.value,

        );

    }

    render() {

        const { id, nom, prenom, mail, date, role } = this.props.currentUser;

        $('#nomEdit').val(nom);

        $('#prenomEdit').val(prenom);

        $('#emailEdit').val(mail);

        const newDate = new Date(date);

        const dateFormat = newDate.getFullYear() + '-' + newDate.getDate() + '-' + (1 + newDate.getMonth()).toString().padStart(2, '0');

        $('#dateEdit').val(dateFormat);

        $('#pwdEdit').val();

        $('#pwdConfEdit').val();

        return (

            <div className="modal fade" id="formModal" tabIndex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="formModalLabel">Modal title</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="alert alert-warning" role="alert" id='alertConfPwdEdit'>
                                Les mots de passe sont différents
                            </div>
                            <form onSubmit={this.editUserSubmit}>
                                <div className="form-group">
                                    <label htmlFor="nom">Nom</label>
                                    <input ref={nomEditInput => this.nomEditInput = nomEditInput} type="text" className="form-control" id="nomEdit" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="prenom">Prenom</label>
                                    <input ref={prenomEditInput => this.prenomEditInput = prenomEditInput} type="text" className="form-control" id="prenomEdit" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Adresse email</label>
                                    <input ref={mailEditInput => this.mailEditInput = mailEditInput} type="email" className="form-control" id="emailEdit" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="date">Date de naissance</label>
                                    <input ref={dateEditInput => this.dateEditInput = dateEditInput} type="date" className="form-control" id="dateEdit" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="pwd">Password</label>
                                    <input ref={pwdEditInput => this.pwdEditInput = pwdEditInput} type="password" className="form-control" id="pwdEdit" placeholder="Password" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="pwdConf">Password confirmation</label>
                                    <input ref={pwdConfEditInput => this.pwdConfEditInput = pwdConfEditInput} type="password" className="form-control" id="pwdConfEdit" placeholder="Password confirmation" required />
                                </div>
                                <button type="submit" className="btn btn-dark pull-left">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )

    }

}