export default class UserItem extends React.Component {

    constructor(props) {

        super(props);
        this.state = { isEdit: false }
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);

    }

    deleteUser() {
        const { id } = this.props.user;
        this.props.deleteUser(id);
    }

    editUser() {

        this.props.editUser(this.props.user);

    }

    render() {

        const { id, nom, prenom, mail, date, role } = this.props.user;

        return (

            <tr key={this.props.index}>
                <td>{id}</td>
                <td>{nom}</td>
                <td>{prenom}</td>
                <td>{mail}</td>
                <td>{date}</td>
                <td>{role}</td>
                <td><img src="https://img.icons8.com/material-sharp/24/000000/edit-file.png" onClick={this.editUser} /></td>
                <td><img src="https://img.icons8.com/metro/26/000000/delete-sign.png" onClick={this.deleteUser} /></td>
            </tr>

        );
    }
}