var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserItem = function (_React$Component) {
    _inherits(UserItem, _React$Component);

    function UserItem(props) {
        _classCallCheck(this, UserItem);

        var _this = _possibleConstructorReturn(this, (UserItem.__proto__ || Object.getPrototypeOf(UserItem)).call(this, props));

        _this.state = { isEdit: false };
        _this.editUser = _this.editUser.bind(_this);
        _this.deleteUser = _this.deleteUser.bind(_this);

        return _this;
    }

    _createClass(UserItem, [{
        key: "deleteUser",
        value: function deleteUser() {
            var id = this.props.user.id;

            this.props.deleteUser(id);
        }
    }, {
        key: "editUser",
        value: function editUser() {

            this.props.editUser(this.props.user);
        }
    }, {
        key: "render",
        value: function render() {
            var _props$user = this.props.user,
                id = _props$user.id,
                nom = _props$user.nom,
                prenom = _props$user.prenom,
                mail = _props$user.mail,
                date = _props$user.date,
                role = _props$user.role;


            return React.createElement(
                "tr",
                { key: this.props.index },
                React.createElement(
                    "td",
                    null,
                    id
                ),
                React.createElement(
                    "td",
                    null,
                    nom
                ),
                React.createElement(
                    "td",
                    null,
                    prenom
                ),
                React.createElement(
                    "td",
                    null,
                    mail
                ),
                React.createElement(
                    "td",
                    null,
                    date
                ),
                React.createElement(
                    "td",
                    null,
                    role
                ),
                React.createElement(
                    "td",
                    null,
                    React.createElement("img", { src: "https://img.icons8.com/material-sharp/24/000000/edit-file.png", onClick: this.editUser })
                ),
                React.createElement(
                    "td",
                    null,
                    React.createElement("img", { src: "https://img.icons8.com/metro/26/000000/delete-sign.png", onClick: this.deleteUser })
                )
            );
        }
    }]);

    return UserItem;
}(React.Component);

export default UserItem;