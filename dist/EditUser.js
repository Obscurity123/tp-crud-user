var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EditUser = function (_React$Component) {
    _inherits(EditUser, _React$Component);

    function EditUser(props) {
        _classCallCheck(this, EditUser);

        var _this = _possibleConstructorReturn(this, (EditUser.__proto__ || Object.getPrototypeOf(EditUser)).call(this, props));

        _this.editUserSubmit = _this.editUserSubmit.bind(_this);

        return _this;
    }

    _createClass(EditUser, [{
        key: 'editUserSubmit',
        value: function editUserSubmit() {

            this.props.editUserSubmit(this.nomEditInput.value, this.prenomEditInput.value, this.mailEditInput.value, this.dateEditInput.value, this.pwdEditInput.value, this.pwdConfEditInput.value);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props$currentUser = this.props.currentUser,
                id = _props$currentUser.id,
                nom = _props$currentUser.nom,
                prenom = _props$currentUser.prenom,
                mail = _props$currentUser.mail,
                date = _props$currentUser.date,
                role = _props$currentUser.role;


            $('#nomEdit').val(nom);

            $('#prenomEdit').val(prenom);

            $('#emailEdit').val(mail);

            var newDate = new Date(date);

            var dateFormat = newDate.getFullYear() + '-' + newDate.getDate() + '-' + (1 + newDate.getMonth()).toString().padStart(2, '0');

            $('#dateEdit').val(dateFormat);

            $('#pwdEdit').val();

            $('#pwdConfEdit').val();

            return React.createElement(
                'div',
                { className: 'modal fade', id: 'formModal', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'formModalLabel', 'aria-hidden': 'true' },
                React.createElement(
                    'div',
                    { className: 'modal-dialog', role: 'document' },
                    React.createElement(
                        'div',
                        { className: 'modal-content' },
                        React.createElement(
                            'div',
                            { className: 'modal-header' },
                            React.createElement(
                                'h5',
                                { className: 'modal-title', id: 'formModalLabel' },
                                'Modal title'
                            ),
                            React.createElement(
                                'button',
                                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                                React.createElement(
                                    'span',
                                    { 'aria-hidden': 'true' },
                                    '\xD7'
                                )
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'modal-body' },
                            React.createElement(
                                'div',
                                { className: 'alert alert-warning', role: 'alert', id: 'alertConfPwdEdit' },
                                'Les mots de passe sont diff\xE9rents'
                            ),
                            React.createElement(
                                'form',
                                { onSubmit: this.editUserSubmit },
                                React.createElement(
                                    'div',
                                    { className: 'form-group' },
                                    React.createElement(
                                        'label',
                                        { htmlFor: 'nom' },
                                        'Nom'
                                    ),
                                    React.createElement('input', { ref: function ref(nomEditInput) {
                                            return _this2.nomEditInput = nomEditInput;
                                        }, type: 'text', className: 'form-control', id: 'nomEdit', required: true })
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'form-group' },
                                    React.createElement(
                                        'label',
                                        { htmlFor: 'prenom' },
                                        'Prenom'
                                    ),
                                    React.createElement('input', { ref: function ref(prenomEditInput) {
                                            return _this2.prenomEditInput = prenomEditInput;
                                        }, type: 'text', className: 'form-control', id: 'prenomEdit', required: true })
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'form-group' },
                                    React.createElement(
                                        'label',
                                        { htmlFor: 'email' },
                                        'Adresse email'
                                    ),
                                    React.createElement('input', { ref: function ref(mailEditInput) {
                                            return _this2.mailEditInput = mailEditInput;
                                        }, type: 'email', className: 'form-control', id: 'emailEdit', required: true })
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'form-group' },
                                    React.createElement(
                                        'label',
                                        { htmlFor: 'date' },
                                        'Date de naissance'
                                    ),
                                    React.createElement('input', { ref: function ref(dateEditInput) {
                                            return _this2.dateEditInput = dateEditInput;
                                        }, type: 'date', className: 'form-control', id: 'dateEdit', required: true })
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'form-group' },
                                    React.createElement(
                                        'label',
                                        { htmlFor: 'pwd' },
                                        'Password'
                                    ),
                                    React.createElement('input', { ref: function ref(pwdEditInput) {
                                            return _this2.pwdEditInput = pwdEditInput;
                                        }, type: 'password', className: 'form-control', id: 'pwdEdit', placeholder: 'Password', required: true })
                                ),
                                React.createElement(
                                    'div',
                                    { className: 'form-group' },
                                    React.createElement(
                                        'label',
                                        { htmlFor: 'pwdConf' },
                                        'Password confirmation'
                                    ),
                                    React.createElement('input', { ref: function ref(pwdConfEditInput) {
                                            return _this2.pwdConfEditInput = pwdConfEditInput;
                                        }, type: 'password', className: 'form-control', id: 'pwdConfEdit', placeholder: 'Password confirmation', required: true })
                                ),
                                React.createElement(
                                    'button',
                                    { type: 'submit', className: 'btn btn-dark pull-left' },
                                    'Submit'
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return EditUser;
}(React.Component);

export default EditUser;