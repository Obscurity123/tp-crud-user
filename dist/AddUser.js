var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AddUser = function (_React$Component) {
    _inherits(AddUser, _React$Component);

    function AddUser(props) {
        _classCallCheck(this, AddUser);

        var _this = _possibleConstructorReturn(this, (AddUser.__proto__ || Object.getPrototypeOf(AddUser)).call(this, props));

        _this.addNewUser = _this.addNewUser.bind(_this);

        return _this;
    }

    _createClass(AddUser, [{
        key: "addNewUser",
        value: function addNewUser() {

            this.props.addNewUser(this.nomInput.value, this.prenomInput.value, this.mailInput.value, this.dateInput.value, this.pwdInput.value, this.pwdConfInput.value);
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement(
                "form",
                { onSubmit: this.addNewUser },
                React.createElement(
                    "div",
                    { className: "form-group" },
                    React.createElement(
                        "label",
                        { htmlFor: "nom" },
                        "Nom"
                    ),
                    React.createElement("input", { ref: function ref(nomInput) {
                            return _this2.nomInput = nomInput;
                        }, type: "text", className: "form-control", id: "nom", placeholder: "Entrer votre nom", required: true })
                ),
                React.createElement(
                    "div",
                    { className: "form-group" },
                    React.createElement(
                        "label",
                        { htmlFor: "prenom" },
                        "Prenom"
                    ),
                    React.createElement("input", { ref: function ref(prenomInput) {
                            return _this2.prenomInput = prenomInput;
                        }, type: "text", className: "form-control", id: "prenom", placeholder: "Entrer votre prenom", required: true })
                ),
                React.createElement(
                    "div",
                    { className: "form-group" },
                    React.createElement(
                        "label",
                        { htmlFor: "email" },
                        "Adresse email"
                    ),
                    React.createElement("input", { ref: function ref(mailInput) {
                            return _this2.mailInput = mailInput;
                        }, type: "email", className: "form-control", id: "email", placeholder: "Entrer votre adresse mail", required: true })
                ),
                React.createElement(
                    "div",
                    { className: "form-group" },
                    React.createElement(
                        "label",
                        { htmlFor: "date" },
                        "Date de naissance"
                    ),
                    React.createElement("input", { ref: function ref(dateInput) {
                            return _this2.dateInput = dateInput;
                        }, type: "date", className: "form-control", id: "date", placeholder: "Entrer votre date de naissance", required: true })
                ),
                React.createElement(
                    "div",
                    { className: "form-group" },
                    React.createElement(
                        "label",
                        { htmlFor: "pwd" },
                        "Password"
                    ),
                    React.createElement("input", { ref: function ref(pwdInput) {
                            return _this2.pwdInput = pwdInput;
                        }, type: "password", className: "form-control", id: "pwd", placeholder: "Password", required: true })
                ),
                React.createElement(
                    "div",
                    { className: "form-group" },
                    React.createElement(
                        "label",
                        { htmlFor: "pwdConf" },
                        "Password confirmation"
                    ),
                    React.createElement("input", { ref: function ref(pwdConfInput) {
                            return _this2.pwdConfInput = pwdConfInput;
                        }, type: "password", className: "form-control", id: "pwdConf", placeholder: "Password confirmation", required: true })
                ),
                React.createElement(
                    "button",
                    { type: "submit", className: "btn btn-dark pull-left" },
                    "Submit"
                )
            );
        }
    }]);

    return AddUser;
}(React.Component);

export default AddUser;